## Prerequisite

- PHP 5.5+
- MySQL Server
- composer
- php-mbstring
- php-dom

#### For Ubuntu user
```
$ sudo apt install composer php-mbstring php-dom
```


## Setup

### Setup MySQL Server
Create MySQL user and dabatase to be used with this application.

### Copy files from the repository
```
$ git clone https://gitlab.com/owasp-ad/ad_web_sendai2.git
$ cd ad_web_sendai2
```

### Edit .env file
Copy ad_owasp_sendai2/.env.sample file to .env. Then edit .env as required.
```
$ cp -n .env.sample .env
$ vim .env
```

### Setup WEB Server
Point document root to ad_web_sendai2/public

### Install files
```
$ composer install
$ php artisan migrate
```

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="card card-default">
                <div class="card-body">
                    <div class="card-title">{{ __('messages.news_add') }}</div>
                    <form method="post">
                    <div class="form-group">
                        <label for="title">{{ __('messages.news_title') }}</label>
                        <input type="title" name="title" class="form-control" id="title">
                    </div>
                    <div class="form-group">
                        <label for="body">{{ __('messages.news_content') }}</label>
                        <input type="body" name="body" class="form-control" id="body">
                    </div>
                    <input type="submit" class="btn btn-block btn-primary" value="{{ __('messages.news_regist') }}">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
<?php
$dirs = explode("\n", shell_exec("ls news"));
foreach($dirs as $dir):?>
<?php if($dir != ''): ?>
<div class="card card-default">
    <div class="card-body">
        <div class="card-title"><?= urldecode(trim($dir)); ?></div>
        <?= file_get_contents('news/' . $dir); ?>
        <a href="/home/news/delete/<?= $dir ?>" class="btn btn-block btn-danger">{{ __('messages.news_delete') }}</a>
    </div>
</div>
<?php endif; ?>
<?php endforeach; ?>
        </div>
    </div>
</div>
@endsection
